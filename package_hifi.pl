#!/usr/bin/env perl
#===============================================================================
#
#         FILE: package_hifi.pl
#
#        USAGE: ./package_hifi.pl  
#
#  DESCRIPTION: Packages a HiFi program
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 05/19/2019 06:00:54 PM
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use Getopt::Long;
use File::Temp;
use File::Copy;
use Cwd qw(abs_path);
use File::Basename qw(dirname);

my $opt_verbose;
my $opt_debug_libs;

my $vcpkg_dir;
my $destdir = "/tmp/out";
my $wrapper_script = dirname(abs_path($0)) . "/wrapper";
my $help;
my $root;
my $copy_system_libs;

my @system_paths = (
	qr/^\/lib/,
	qr/^\/usr/
);

my @excluded_system_libs = (
	qr/glibc/
);

my @exclude = (
	qr/^CMake/,
	qr/^cmake/,
	qr/cmake$/,
	qr/Makefile/,
	qr/.h$/,
	qr/_autogen$/
);

# TODO: Maybe autogenerate this
my @ext_lib_paths = qw(
	ext/makefiles/steamworks/project/src/steamworks/public/steam/lib/linux64/
	ext/makefiles/steamworks/project/src/steamworks/redistributable_bin/linux64/
	ext/makefiles/polyvox/project/lib/RelWithDebInfo/
	ext/makefiles/quazip/project/lib/
);

GetOptions(
	"verbose|v"           => \$opt_verbose,
	"destdir|d=s"         => \$destdir,
    "vcpkgdir=s"          => \$vcpkg_dir,
    "debug-libs"          => \$opt_debug_libs,
	"wrapper|w=s"         => \$wrapper_script,
    "help|h"              => \$help,
	"root=s"              => \$root,
	"copy-system-libs"    => \$copy_system_libs,
) or die "Getopt failed: $!";


if ( $help ) {
	print <<HELP;
$0: Package High Fidelity code.

This quick and dirty script starts from a build directory and creates a new
directory that contains a binary, dependencies and required files, while
excluding unneeded ones.

Without --copy-system-libs it will only copy the libraries not found in
system paths, creating a distro and distro version specific package.

With --copy-system-libs all dependencies will be included, which should
produce an universal, but noticeably larger package.

Options:
	-v, --verbose            Verbose operation
    -d, --destdir dir        Where to write the results
	--vcpkgdir dir           Directory with vcpkg data
    --debug-libs             Use the debug version of vcpkg libs
    -w, --wrapper file       Wrapper to script to use
    -h, --help               Show this help
    --root root              Root of the build directory (autodetected)
    --copy-system-libs       Copy libraries found in system directories
HELP

	exit(0);
}
my $program = shift @ARGV;
$| = 1;



my %system_deps;
my %other_deps;
my %seen;

inf("Starting...");

collect_dependencies($program);
make_package();


sub err {
	print STDERR "ERR: " . shift . "\n";
}

sub inf {
	print STDERR "INF: " . shift . "\n";
}

sub dbg {
	print STDERR "DBG: " . shift . "\n" if ($opt_verbose);
}

sub find_root {
	my ($start_path) = @_;
	my $ret = abs_path($start_path);
	while( !(-f "$ret/CMakeCache.txt" && -d "$ret/ext") && ($ret ne "/") ) {
		$ret = abs_path("$ret/..");		
	}

	if ( $ret eq "/" ) {
		die "Can't find the root of the build directory";
	}

	return $ret;
}

sub get_library_paths {
	my $ret;

	foreach my $dir (@ext_lib_paths) {
		$ret .= ":" if ($ret);
		$ret .= "$root/$dir";

		if ( ! -d "$root/$dir" ) {
			err("Can't find library path: $root/$dir");
		}
	}

	return $ret;
}


sub ldd {
	my ($binary) = @_;
	my @deps;

	inf("=== Starting ===");
	$root = find_root( dirname($program) );
	dbg("root: $root");

	inf("=== Setting library paths ===");
	my $libpath = get_library_paths();
	$ENV{LD_LIBRARY_PATH}=$libpath;
	dbg($libpath);

	inf("=== Dependencies for $binary ===");

	open(my $fh, "-|", "ldd", $binary) or die "Can't run ldd $binary: $!";
	while(my $line = <$fh> ) {
		chomp $line;

		next unless ($line =~ /=>/);
		my ($soname, $path) = split(/\s*=>\s*/, $line);
		$soname =~ s/^\s+//;
		$soname =~ s/\s+$//;


		$path =~ s/\s+\([0-9a-fA-Fx]*\)//;

		dbg("$soname => $path");

		if ( $path eq "not found" ) {
			err("Can't find library '$soname' for binary '$binary'");
		}

		push @deps, $path;
	}

	close $fh;

	dbg("=== END ===");

	return @deps;

}

sub is_system {
	my ($path) = @_;

	if ( $copy_system_libs ) {
		foreach my $re (@excluded_system_libs) {
			return 1 if ( $path =~ /$re/ )
		}
	} else {
		foreach my $re (@system_paths) {
			return 1 if ( $path =~ /$re/ )
		}
	}
	return 0;
}

sub is_excluded {
	my ($path) = @_;
	foreach my $re (@exclude) {
		return 1 if ( $path =~ /$re/ )
	}

	return 0;
}


sub collect_dependencies {
	my ($binary) = @_;
	return if exists ($seen{$binary});
	$seen{$binary} = 1;


	my @deps = ldd($binary);
	foreach my $dep (@deps) {
		if ( is_system($dep) ) {
			$system_deps{$dep} = 1;
		} else {
			$other_deps{$dep} = 1;
			collect_dependencies($dep);
		}
	}
}


sub get_package_dependencies {
	my %pkg;
	foreach my $file (keys %system_deps) {
		my $pkgname = `rpm -qf "$file"`;
		chomp $pkgname;

		if ( $? == 0 ) {
			$pkg{$pkgname} = 1;
		}
	}

	return keys %pkg;
}


sub find_all_libs {
	my ($dir) = @_;
	my @ret;

	opendir(my $dh, $dir) or die "Can't opendir $dir: $!";
	while(my $de = readdir($dh)) {
		next if ($de eq "." || $de eq "..");
		if ( -d "$dir/$de" ) {
			push @ret, find_all_libs("$dir/$de");
		} else {
			push @ret, "$dir/$de" if ( $de =~ /\.so$/ || $de =~ /.so\.\d+$/ );
		}
	}
	closedir($dh);

	return @ret;
}


sub get_vcpkg_dependencies {
	my $subdir = "";
	if ( !$vcpkg_dir ) {
		inf("vcpkg directory not defined, not packaging vcpkg libs");
		return;
	}

	opendir(my $dir, $vcpkg_dir) or die "Can't opendir $vcpkg_dir: $!";
	while(my $de = readdir($dir)) {
		if ( $de =~ /^[a-fA-F0-9]+$/ ) {
			$subdir = $de;
			last;
		}
	}
	closedir($dir);

	if (!$subdir) {
		err("Failed to find vcpkg subdirectory");
		return;
	}

	if ( $opt_debug_libs ) {
		return find_all_libs("$vcpkg_dir/$subdir/installed/x64-linux/debug/lib");
	}  else {
		return find_all_libs("$vcpkg_dir/$subdir/installed/x64-linux/lib");
	}
}

sub make_package {
	# TODO: replace with proper temp dir

	inf("=== Copying project ===");
	copy_dir(".", $destdir);

	inf("=== Copying libraries ===");	
	mkdir("$destdir/lib");
	foreach my $lib (keys %other_deps) {
		dbg("$lib => $destdir/lib/");
		copy($lib, "$destdir/lib/");
	}

	if (!$copy_system_libs) {
		inf("=== Generating packaged dependencies ===");
		open(my $fh, ">", "$destdir/required_packages.txt");
		print $fh join("\n", get_package_dependencies()) . "\n";
		close $fh;
	}

	inf("=== Generating vcpkg dependencies ===");
	my @libs = get_vcpkg_dependencies();
	foreach my $lib (@libs) {
		dbg("$lib");
		copy($lib, "$destdir/lib/");
	}

	inf("=== Setting up wrapper ===");
	if ( -f $wrapper_script ) {
		mkdir("$destdir/bin");
		rename("$destdir/$program", "$destdir/bin/$program");
		copy($wrapper_script, "$destdir/$program");
		chmod 0755, "$destdir/$program", "$destdir/bin/$program";
	} else {
		err("Wrapper script '$wrapper_script' not found, skipping");
		chmod 0755, "$destdir/$program";
	}

}

sub copy_dir {
	my ($dir, $dest) = @_;
	mkdir($dest);


	opendir(my $dh, $dir) or die "Can't opendir $dir: $!";
	while(my $file = readdir($dh)) {
		next if ($file eq "." || $file eq "..");
		next if (is_excluded($file));
		if ( -f "$dir/$file" ) {
			dbg("$dir/$file => $dest/$file");

			copy("$dir/$file", "$dest/$file") or die "Failed to copy $dir/$file to $dest/$file: $!";
		} elsif ( -d "$dir/$file" ) {
			copy_dir("$dir/$file", "$dest/$file");
		}
	}

	closedir($dh);
}

